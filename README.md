# Ethan后端微服务公共包
本公共包仅用于hyperf2.0框架，请开发同学先熟悉hyperf相关文档

## 一、项目结构
~~~bash
├── Aspect #切面管理
├── Common #公共管理
├── Constants #常量管理
├── Controller #控制层初始化
├── EsData #es相关初始化
├── Exception #异常服务相关
├── JsonRpc #rpc服务
├── Listener #监听器
├── Middleware #中间件
├── Model #模型初始化
├── Redis #redis初始化
├── Request #验证初始化
└── Utils # 工具集
~~~

## 二、安装
1）、在自己业务项目composer.json中添加以下内容
```json
  "ethan-z/hyperf-ext": "1.*",
```

2）、之后执行 `composer update`,

## 三、新建业务错误枚举类（每个抛错或日志都必须唯一）
    | 服务（三位）.模块（三位）.code（3位）
    | APP_ID（三位）.模块id（三位）.code（三位）
    | 中文可重复定义，每个报错对应一个code，方便定位
    | 如：ORD_OUT_001 = [200100001, '订单出库失败'];

## 四、创建Model
### 1、生成model
  `./bin/hyperf.php gen:model order_after_sales --table-mapping order_after_sales:OrderAfterSalesModel --refresh-fillable --path app/Model --with-comments`

### 2、自动生成分表
a) 在model内自定义分表粒度（默认按年分表），如：public string $subTableSuffixFormat = 'Ym'
b) 查询时使用setSuffix、getModelBySnack则会自动生成分表

### 3、获取雪花id
```php
make(IdGeneratorInterface::class)->generate();
```

### 4、根据当前雪花id生成新雪花id
```php
Tools::getSfIdByOther();
```

## 五、链路追踪
  在env配置APP_ENV != prod则自动开启链路追踪


## 六、异常处理
使用示例1：
```php
throw new BaseException(Code::ERROR, 1, $e);
```

使用示例2：
```php
throw new BaseException(Code::ERROR);
```

## 七、RPC服务
### 1、首先在服务创建rpc，如：
`JsonRpc-》Server-》UserServiceServer.php`

### 2、创建远程访问，如：
`JsonRpc-》Client-》UserServiceClient.php`

`JsonRpc-》Inter-》UserServiceInter.php`

### 3、创建rpc依赖关系

在Utils-》LoadRpc.php新增依赖关系

在env新增配置，如：

USER_SERVICE_SERVER_HOST=127.0.0.1

USER_SERVICE_SERVER_PORT=11001



## 八、数据校验
### 1、创建验证
a) `必须继承BaseRequest`

b) `添加相应方法的场景`

b) `添加验证规则`：

  像id、time等相同的验证最好使用BaseRequest的通用验证

  像page、pageSize等必要验证不需要写入验证规则内，其会自动进行验证
  
c) `自定义通用规则`：

  在Utils-》Rules.php、Attributes.php进行新增验证，在Listener-》ValidatorFactoryResolvedListener.php进行引入验证规则
  
  现有验证：phone、pwd、is_unique

d) `在控制器使用`
```php
$this->commonValidate();
```

e) `获取验证参数`
```php
$this->request->validated();
```

f) `其他地方使用`
```php
make(MarketingRequest::class)->scene('monthlyTasks')->validateResolved();
```


## 九、封装的Es抽象类使用
使用示例
```php
make(OrderAfterSalesElasticSearch::class)->createEsIndex([]);
make(OrderAfterSalesElasticSearch::class)->deleteIndex([]);
make(OrderAfterSalesElasticSearch::class)->normalSearch([]);
make(OrderAfterSalesElasticSearch::class)->normalSearch([]);
...
```


## 十、redis使用
### 1、每个服务必须在Constants下新建文件，并按照 public const INCR_NO = ['ino:', 60]形式申明
### 2、每个服务必须在Redis下新建文件，并继承BaseRedis
### 3、使用示例
```php
make(CommonRedis::class)
   ->setKey(CommonRedisName::INCR_NO, '121')
   ->incr();
```


## 十一、工具类
在Utils-》Tools.php进行新增


## 十二、全局配置
在ConfigProvider.php进行新增