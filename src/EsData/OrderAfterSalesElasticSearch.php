<?php

namespace EthanZ\HyperfExt\EsData;

/**
 * 售后信息es
 */
class OrderAfterSalesElasticSearch extends ElasticSearchBase
{

    /**
     * ES数据类型
     *
     *
     * @return string[]
     */
    public function dbToEsMapping(): array
    {
        return [
            'id'                        => 'int',
            'sfId'                      => 'bigint',
            'userId'                    => 'int',
            'slaveUserId'               => 'int',
            'afterSalesNumber'          => 'varchar',
            'typeVal'                   => 'int',
            'classify'                  => 'int',
            'orderSfId'                 => 'bigint',
            'orderNumber'               => 'varchar',
            'status'                    => 'int',
            'verifyStep'                => 'int',
            'lastReviewTime'            => 'int',
            'hadAll'                    => 'int',
            'serviceType'               => 'int',
            'afterType'                 => 'int',
            'reasonId'                  => 'int',
            'sourceMode'                => 'int',
            'orderPrice'                => 'float',
            'finalPrice'                => 'float',
            'afterPrice'                => 'float',
            'cashPrice'                 => 'float',
            'afterCashPrice'            => 'float',
            'rechargeBalancePrice'      => 'float',
            'afterRechargeBalancePrice' => 'float',
            'presentBalancePrice'       => 'float',
            'afterPresentBalancePrice'  => 'float',
            'creditPrice'               => 'float',
            'afterCreditPrice'          => 'float',
            'freight'                   => 'float',
            'description'               => 'varchar',
            'damageStatus'              => 'int',
            'damageOrEmpty'             => 'int',
            'finishedTime'              => 'int',
            'refundMode'                => 'int',
            'extractStatus'             => 'int',
            'refundMoneyFirstTime'      => 'int',
            'refundMoneyLastTime'       => 'int',
            'refundMoney'               => 'float',
            'type'                      => 'int',
            'exchangeOrderNumber'       => 'varchar',
            'erpStatus'                 => 'int',
            'createdTime'               => 'int',
            'createdUser'               => 'int',
            'updatedTime'               => 'int',
            'updatedUser'               => 'int',
            'deletedTime'               => 'int',
            'deletedUser'               => 'int',
            // 售后详情.
            'orderAfterSalesDetails'    => 'array',
        ];
    }


    /**
     * 设置索引名
     *
     *
     *
     * @return string
     */
    public function indexName(): string
    {
        return config('databases.default.prefix') . 'order_after_sales_' . env('APP_ID');
    }

    /**
     * 设置对应表名
     *
     *
     * @return array
     */
    public function setTableName(): array
    {
        return [];
    }

    /**
     * 重建索引数据
     *
     * @param int $per
     *
     * @return bool
     */
    public function reIndexData(int $per = 100): bool
    {
        return true;
    }

    /**
     * 普通查询
     *
     *
     * @param $filter
     * @param $page
     * @param $pageSize
     *
     * @return array
     */
    public function normalSearch($filter, $page = 1, $pageSize = 20): array
    {
        $paramData = [
            'index' => $this->indexName(),
            'from'  => ($page - 1) * $pageSize,
            'size'  => $pageSize,
        ];
        if ($filter) {
            $paramData['body'] = [
                'query' => [
                    'bool' => [
                        'must' => $filter,
                    ],
                ],
            ];
        }
        $list = $this->getEsClient()->search($paramData);

        return $this->resolveQueryListResult($list);
    }

    /**
     * 根据关键字获取
     *
     * @param int    $userId
     * @param int    $slaveUserId
     * @param string $keyword
     * @param array  $timeBetween
     *
     * @return array
     */
    public function getByKeyword(int $userId, int $slaveUserId, string $keyword, int $page = 1, int $pageSize = 20, array $timeBetween = []): array
    {
        $must   = [
            ['term' => ['userId' => $userId]],
            ['term' => ['slaveUserId' => $slaveUserId]],
        ];
        $should = [];
        if ($keyword) {
            $should = [
                ['match_phrase' => ['orderAfterSalesDetails.goodsName' => $keyword]],
                ['match_phrase' => ['afterSalesNumber' => $keyword]],
                ['match_phrase' => ['orderNumber' => $keyword]],
            ];
        }
        $where  = [
            'bool' => [
                'must' => $must,
            ]
        ];
        $filter = [];
        if ($timeBetween) {
            $filter = [
                'range' => [
                    'createdTime' => ['gte' => $timeBetween[0], 'lt' => $timeBetween[1]]
                ]
            ];
        }
        if ($filter) {
            $where['bool']['filter'] = $filter;
        }
        if ($should) {
            $where['bool']['should']               = $should;
            $where['bool']['minimum_should_match'] = 1;
        }
        $param = [
            'index' => $this->indexName(),
            'body'  => [
                'query' => $where,
                'sort'  => [
                    ['id' => ['order' => 'desc']],
                ],
                'from'  => ($page - 1) * $pageSize,
                'size'  => $pageSize,
            ]
        ];
        $list  = $this->getEsClient()->search($param);

        return $this->resolveQueryListResult($list);
    }
}
