<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

#[Constants]
class UserRedisName extends AbstractConstants
{


    // 用户信息.
    public const USER_INFO = ['uif:', 7 * 86400];
}
