<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

#[Constants]
class Code extends AbstractConstants
{


    /*
    |--------------------------------------------------------------------------
    | 公共code
    |--------------------------------------------------------------------------
    |
    | CODE名：服务（三位）.模块（三位）.code（3位）
    | CODE码：APP_ID（三位）.模块id（三位）.code（三位）
    | 中文可重复定义，每个报错对应一个code，方便定位
    |
    */
    public const SUCCESS = [1, '成功'];
    public const ERROR   = [0, '失败'];

    public const ORD_OUT_001   = [200100001, '订单出库失败'];
}
