<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

#[Constants]
class CommonRedisName extends AbstractConstants
{


    // 递增数.
    public const INCR_NO = ['ino:', 60];

    // 用户锁.
    public const USER_LOCK = ['ulc:', 3600];
}
