<?php
declare(strict_types=1);

namespace EthanZ\HyperfExt\JsonRpc\Inter;

interface UserServiceInter
{


    public function getUserInfo(int $userId) : array;
}