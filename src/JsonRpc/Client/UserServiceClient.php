<?php
declare(strict_types=1);

namespace EthanZ\HyperfExt\JsonRpc\Client;

use EthanZ\HyperfExt\JsonRpc\Inter\UserServiceInter;
use Hyperf\RpcClient\AbstractServiceClient;

class UserServiceClient extends AbstractServiceClient implements UserServiceInter
{


    /**
     * 服务提供者的服务名称
     * @var string
     */
    protected $serviceName = "UserServiceServer";


    /**
     * 定义对应服务提供者的服务协议
     * @var string
     */
    protected $protocol = 'jsonrpc-http';


    public function getUserInfo(int $userId) : array
    {
        return $this->__request(__FUNCTION__, compact('userId'));
    }
}