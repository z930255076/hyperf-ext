<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Log;

use Hyperf\Amqp\Producer;
use Hyperf\Utils\ApplicationContext;
use Monolog\Handler\AbstractProcessingHandler;
use EthanZ\HyperfExt\Common\TraceEntity;

class LogHandler extends AbstractProcessingHandler
{
    public function handle(array $record): bool
    {
        $systemLogEnable = config('system_log_enable', env('SYSTEM_LOG_ENABLE'));
        if (1) {
            // sql记录开关判断.
            $sqlLogEnAble = config('sql_log_enable', env('SQL_LOG_ENABLE'));
            if ($record['channel'] == 'sql' && !$sqlLogEnAble) {
                return parent::handle($record);
            }
            $log = [
                'log_type'   => 'system_log',
                'datetime'   => date('Y-m-d H:i:s', time()),
                'app_id'     => env('APP_ID'),
                'app_name'   => env('APP_NAME'),
                'channel'    => isset($record['channel']) ? $record['channel'] : '',
                'msg'        => isset($record['message']) ? $record['message'] : '',
                'context'    => isset($record['context']) ? is_string($record['context']) ? [$record['context']] : $record['context'] : '',
                'level'      => isset($record['level']) ? $record['level'] : '',
                'level_name' => isset($record['level_name']) ? $record['level_name'] : '',
                'extra'      => isset($record['extra']) ? $record['extra'] : '',
            ];
            echo json_encode($log)."\n";
        }

        return parent::handle($record);
    }

    public function reset()
    {
        parent::reset();
    }

    protected function write(array $record): void
    {
    }
}