<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Listener;

use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\Event\ValidatorFactoryResolved;

/**
 * @Listener
 */
class ValidatorFactoryResolvedListener implements ListenerInterface
{


    public function listen(): array
    {
        return [
            ValidatorFactoryResolved::class,
        ];
    }


    public function process(object $event)
    {
        /**  @var ValidatorFactoryInterface $validatorFactory */
        $validatorFactory = $event->validatorFactory;

        // 电话.
        {
            $validatorFactory->extend('phone', 'EthanZ\HyperfExt\Utils\Rules@phone');
            $validatorFactory->replacer('phone', 'EthanZ\HyperfExt\Utils\Attributes@phone');
        }

        // 密码.
        {
            $validatorFactory->extend('pwd', 'EthanZ\HyperfExt\Utils\Rules@pwd');
            $validatorFactory->replacer('pwd', 'EthanZ\HyperfExt\Utils\Attributes@pwd');
        }

        // 唯一字段验证.
        {
            $validatorFactory->extend('is_unique', 'EthanZ\HyperfExt\Utils\Rules@isUnique');
            $validatorFactory->replacer('is_unique', 'EthanZ\HyperfExt\Utils\Attributes@isUnique');
        }
    }
}
