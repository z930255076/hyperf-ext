<?php

namespace EthanZ\HyperfExt\Exception;

use EthanZ\HyperfExt\Constants\Code;
use Hyperf\Server\Exception\ServerException;
use Throwable;

/**
 * 抛出异常.
 *
 * Class BaseException
 */
class BaseException extends ServerException
{


    public int            $needLog = 1;

    public Throwable|null $exception;


    /**
     * CodeException constructor.
     *
     * @param array          $enum
     * @param int            $needLog
     * @param Throwable|null $previous
     */
    public function __construct(array $enum = Code::ERROR, int $needLog = 0, Throwable $previous = null)
    {
        $this->needLog   = $needLog;
        $this->exception = $previous;

        parent::__construct($enum[1], $enum[0], $previous);
    }


    public function needLog(): int
    {
        return $this->needLog;
    }


    public function exception(): null|Throwable
    {
        return $this->exception;
    }

}
