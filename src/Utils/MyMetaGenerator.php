<?php

namespace EthanZ\HyperfExt\Utils;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Snowflake\ConfigurationInterface;
use Hyperf\Snowflake\MetaGenerator\RedisMilliSecondMetaGenerator;
use Hyperf\Snowflake\MetaGeneratorInterface;

class MyMetaGenerator extends RedisMilliSecondMetaGenerator
{


    protected int $nowTime;

    public function __construct(int $nowTime)
    {
        $this->nowTime = $nowTime * 1000;
        $beginTimestamp = (int)(make(MetaGeneratorInterface::class)->getBeginTimestamp() / 1000);

        parent::__construct(
            make(ConfigurationInterface::class),
            $beginTimestamp,
            make(ConfigInterface::class)
        );
    }

    public function getTimestamp(): int
    {
        return $this->nowTime;
    }
}