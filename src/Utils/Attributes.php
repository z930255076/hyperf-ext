<?php

namespace EthanZ\HyperfExt\Utils;

use Hyperf\Validation\ValidatorFactory;

/**
 * 验证错误的自定义属性工具类
 */
class Attributes extends ValidatorFactory
{


    /**
     * 电话
     *
     * @param string $message    自定义提醒.
     * @param string $attribute  要被验证的属性名称.
     * @param mixed  $rule       属性的值.
     * @param array  $parameters 传入验证规则的参数数组.
     *
     * @return mixed
     */
    public function phone(string $message, string $attribute, mixed $rule, array $parameters): string
    {
        return trans('lang.EC01005');
    }


    /**
     * 密码
     *
     * @param string $message    自定义提醒.
     * @param string $attribute  要被验证的属性名称.
     * @param mixed  $rule       属性的值.
     * @param array  $parameters 传入验证规则的参数数组.
     *
     * @return mixed
     */
    public function pwd(string $message, string $attribute, mixed $rule, array $parameters): string
    {
        return trans('lang.EU01008');
    }


    /**
     * 修改数据检查是否存在
     *
     * @param string $message    自定义提醒.
     * @param string $attribute  要被验证的属性名称.
     * @param mixed  $rule       属性的值.
     * @param array  $parameters 传入验证规则的参数数组.
     *
     * @return mixed
     */
    public function isUnique(string $message, string $attribute, mixed $rule, array $parameters): string
    {
        return trans('lang.EU01009');
    }
}
