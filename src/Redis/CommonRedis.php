<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Redis;

use Throwable;

class CommonRedis extends BaseRedis
{


    public function __construct()
    {
        $this->appId = '00';

        parent::__construct();
    }


    /**
     * 自增
     *
     * @return int
     */
    public function incr(): int
    {
        $value = $this->redis->incr($this->key);
        if ($this->timeout && $value == 1) {
            $this->redis->expire($this->key, $this->timeout);
        }

        return $value;
    }


    /**
     * 锁
     *
     * @return bool
     * @throws Throwable
     */
    public function lock(): bool
    {
        return $this->redis->set($this->key, 1, ['nx', 'ex' => $this->timeout]);
    }


    /**
     * 解锁
     *
     * @return int
     */
    public function unLock(): int
    {
        return $this->redis->del($this->key);
    }
}
