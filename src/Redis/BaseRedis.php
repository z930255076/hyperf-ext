<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Redis;

use Hyperf\Di\Annotation\Inject;
use Hyperf\Redis\Redis;

abstract class BaseRedis
{


    /**
     * @var string
     */
    protected string $appId;


    /**
     * 缓存名
     *
     * @var string
     */
    protected string $key;


    /**
     * 超时时间
     *
     * @var int
     */
    protected int $timeout;


    /**
     * @Inject
     * @var Redis
     */
    protected Redis $redis;


    public function __construct()
    {
        $this->redis = make(Redis::class);
    }


    /**
     * 设置缓存名
     *
     * @param array  $dayData
     * @param string $suffix
     *
     * @return BaseRedis
     */
    public function setKey(array $dayData, string $suffix = ''): static
    {
        $this->key     = $this->appId . ':' . $dayData[0];
        $this->timeout = $this->timeout ?? ($dayData[1] ?? 0);
        if ($suffix) {
            $this->key = $this->key . ':' . $suffix;
        }

        return $this;
    }


    /**
     * 设置超时时间
     *
     * @param int $timeout
     *
     * @return $this
     */
    public function setTimeout(int $timeout): static
    {
        $this->timeout = $timeout;

        return $this;
    }


    /**
     * 获取缓存名
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }


    /**
     * 获取缓存
     *
     * @return string|mixed|false|Redis
     */
    public function get(): mixed
    {

        return $this->redis->get($this->key);
    }
}
