<?php

declare(strict_types=1);

namespace EthanZ\HyperfExt\Redis;


class UserRedis extends BaseRedis
{


    public function __construct()
    {
        $this->appId = env('USER_APP_ID');

        parent::__construct();
    }
}
