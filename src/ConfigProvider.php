<?php

namespace EthanZ\HyperfExt;

use EthanZ\HyperfExt\Utils\LoadRpc;
use Monolog\Formatter;
use Monolog\Logger;

class ConfigProvider
{

    public function __invoke(): array
    {
        $dependencies = LoadRpc::rpcDependencies();
        $date         = date('Ym');

        return [
            // 合并到  config/autoload/dependencies.php 文件
            'dependencies' => $dependencies,
            // 合并到  config/autoload/annotations.php 文件
            'annotations'  => [
            ],
            // 默认 Command 的定义，合并到 Hyperf\Contract\ConfigInterface 内，换个方式理解也就是与 config/autoload/commands.php 对应
            'commands'     => [
            ],
            // 与 commands 类似
            'listeners'    => [
                \EthanZ\HyperfExt\Listener\ValidatorFactoryResolvedListener::class,
                \EthanZ\HyperfExt\Listener\DbQueryExecutedListener::class,
            ],
            // 日志
            'logger'      => [
                // todo
                'default' => [
                    'handler' => [
                        'class' => \EthanZ\HyperfExt\Log\LogHandler::class,
                        'constructor' => [],
                    ],
                    'formatter' => [
                        'class' => \Monolog\Formatter\LineFormatter::class,
                        'constructor' => [
                            'format' => null,
                            'dateFormat' => 'Y-m-d H:i:s',
                            'allowInlineLineBreaks' => true,
                        ],
                    ],
                ],
//                'default' => [
//                    'handlers' => [
//                        [
//                            'class' => \Monolog\Handler\RotatingFileHandler::class,
//                            'constructor' => [
//                                'filename' => BASE_PATH . "/runtime/logs/$date/err.log",
//                                'level' => Logger::ERROR,
//                                'maxFiles' => 1,
//                            ],
//                            'formatter' => [
//                                'class' => Formatter\LineFormatter::class,
//                                'constructor' => [
//                                    'format' => null,
//                                    'dateFormat' => 'Y-m-d H:i:s',
//                                    'allowInlineLineBreaks' => true,
//                                ],
//                            ],
//                        ]
//                    ],
//                ],
            ],
            'databases'    => [
                'default' => [
                    'commands' => [
                        'gen:model' => [
                            //需要删除掉自己项目中的该配置项，否则会报错误
                            'uses' => \EthanZ\HyperfExt\Model\Model::class
                        ],
                    ],
                ],
            ],
            'middlewares'  => [
                'http' => [
                    \Hyperf\Validation\Middleware\ValidationMiddleware::class,
                    \EthanZ\HyperfExt\Middleware\LogMiddleware::class,
                ],
            ],
        ];
    }
}